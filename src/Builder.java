import java.util.Date;

public class Builder {

    private static WeatherCharacteristics weatherCharacteristics;

    public Builder() {
        weatherCharacteristics =new WeatherCharacteristics();
    }

    public WeatherCharacteristics builder(){
        return weatherCharacteristics;
    }

    public Builder date(Date date) {
        weatherCharacteristics.setDate(date);
        return this;
    }

    public  Builder temperature(Double temperature) {
        weatherCharacteristics.setTemperature(temperature);
        return this;
    }

    public  Builder relativeHumidity(Double relativeHumidity) {
        weatherCharacteristics.setRelativeHumidity(relativeHumidity);
        return this;
    }

    public  Builder windSpeed(Double windSpeed) {
        weatherCharacteristics.setWindSpeed(windSpeed);
        return this;
    }

    public Builder windDirection(Double windDirection) {
        weatherCharacteristics.setWindDirection(windDirection);
        return this;
    }
}
