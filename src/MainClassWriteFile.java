import java.io.File;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

public class MainClassWriteFile {

    //путь до файла с результатами. Вообще не обязателен,но так удобнее искать. Можно поменять, если расположение не слишком удобное
    private static final String pathAnalyticsFile = "src/answer.txt";

    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите путь до файла:");
        String pathFile = scan.next();

        writeInFileInform(pathFile,pathAnalyticsFile);
    }


    public static void writeInFileInform(String pathCsvFileWeather, String answerFile) throws Exception {
        WorkingWithFile fileW = new WorkingWithFile(new File(Paths.get(pathCsvFileWeather).toUri()));
        List<WeatherCharacteristics> weatherList = fileW.readWeather();
        WeatherAnalytics weatherAnalytics = new WeatherAnalytics(weatherList);

        File file = new File(answerFile);
        if (!file.exists()) {
            file.createNewFile();
        }

        PrintWriter writer = new PrintWriter(file);

        writer.println("Средняя температура воздуха: " + weatherAnalytics.averageTemperature());
        writer.println("Средняя влажность: " + weatherAnalytics.averageHumidity());
        writer.println("Средняя скорость ветра: " + weatherAnalytics.averageWindSpeed());

        SimpleDateFormat format = new SimpleDateFormat("dd.MM, HH:mm");

        WeatherCharacteristics maxTemp = weatherAnalytics.maxTemperature();
        writer.println("Самая высокая температура //" + maxTemp.getTemperature() + " °C // была " + format.format(maxTemp.getDate()));

        WeatherCharacteristics minHumidity = weatherAnalytics.minHumidity();
        writer.println("Самая низкая влажность //" + minHumidity.getRelativeHumidity() +" % // наблюдалась " + format.format(minHumidity.getDate()));

        WeatherCharacteristics maxWindSpeed = weatherAnalytics.maxWindSpeed();
        writer.println("Самый сильный ветер //" + maxWindSpeed.getWindSpeed() +" km/h // достигался " + format.format(maxWindSpeed.getDate()));

        writer.println("Частое направление ветра -  " + weatherAnalytics.frequentWindDirection());

        writer.println(" ");
        writer.println("<!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!><!>");
        writer.println("Прогноз погоды с ИТИСом: если неправильный, значит не хватает точки с запятой)))");

        writer.flush();
        writer.close();
    }
}
