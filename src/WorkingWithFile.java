import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class WorkingWithFile {

    private File csvFileWeather;

    public WorkingWithFile(File csvFileWeather) {
        this.csvFileWeather = csvFileWeather;
    }

    public List<WeatherCharacteristics> readWeather() throws Exception {
        List<WeatherCharacteristics> weatherList = new ArrayList<>();

        Scanner scanner = new Scanner(csvFileWeather);
        while (scanner.hasNext()) {

            String line = scanner.nextLine();
            String[] split = line.split(",");
            String firstSplit = split[0];
            char charAt = firstSplit.charAt(0);

            if (charAt >= '0' && charAt <= '9') {
                WeatherCharacteristics weatherCharacter = deserializeWeather(split);
                weatherList.add(weatherCharacter);
            }
        }

        return weatherList;
    }

    private WeatherCharacteristics deserializeWeather(String[] split) throws Exception {
        WeatherCharacteristics weatherCharacteristics = null;
        Builder builder = new Builder();

        if (split.length == 5) {
            String dateStr = split[0];

            SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HHmm");

            Date dateFormat = format.parse(dateStr);
            Double temperature = Double.parseDouble(split[1]);
            Double relativeHumidity = Double.parseDouble(split[2]);
            Double windSpeed = Double.parseDouble(split[3]);
            Double windDirection = Double.parseDouble(split[4]);


            weatherCharacteristics = builder
                    .date(dateFormat)
                    .temperature(temperature)
                    .relativeHumidity(relativeHumidity)
                    .windSpeed(windSpeed)
                    .windDirection(windDirection)
                    .builder();
        }

        return weatherCharacteristics;
    }

}

