import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WeatherAnalytics {

    private List<WeatherCharacteristics> weatherList;


    public WeatherAnalytics(List<WeatherCharacteristics> weatherList) {
        this.weatherList = weatherList;
    }

    //среднее:температура, влажность, скорость ветра

    public Double averageTemperature() {
        return weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getTemperature())
                .average()
                .orElse(0.0);
    }


    public Double averageHumidity() {
        return weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getRelativeHumidity())
                .average()
                .orElse(0.0);
    }


    public Double averageWindSpeed() {
        return weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getWindSpeed())
                .average()
                .orElse(0.0);
    }


    //максимальная температура, минимальная влажность, максимальная скорость ветра, самое популярное направление ветра

    public WeatherCharacteristics maxTemperature() {
        Double maxTemp = weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getTemperature())
                .max()
                .orElse(0.0);
        return weatherList.stream()
                .filter((weatherCharacteristics) -> weatherCharacteristics.getTemperature()
                        .equals(maxTemp))
                .findFirst()
                .orElse(null);
    }

    public WeatherCharacteristics minHumidity() {
        Double minHumidity = weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getRelativeHumidity())
                .min()
                .orElse(0.0);
        return weatherList.stream()
                .filter((weatherCharacteristics) -> weatherCharacteristics.getRelativeHumidity()
                        .equals(minHumidity))
                .findFirst()
                .orElse(null);
    }

    public WeatherCharacteristics maxWindSpeed() {
        Double maxWindSpeed = weatherList.stream()
                .mapToDouble((weatherCharacteristics) -> weatherCharacteristics.getWindSpeed())
                .max()
                .orElse(0.0);
        return weatherList.stream()
                .filter((weatherCharacteristics) -> weatherCharacteristics.getWindSpeed()
                        .equals(maxWindSpeed))
                .findFirst()
                .orElse(null);
    }

    public String frequentWindDirection() {

        List<String> directions = weatherList.stream().map(
                (weatherCharacteristics) -> {double windDirection = weatherCharacteristics.getWindDirection();
                    if (windDirection >= 315 || windDirection < 45) {
                        return "North";
                    } else if (windDirection >= 45 && windDirection < 135) {
                        return "East";
                    } else if (windDirection >= 135 && windDirection < 225) {
                        return "South";
                    } else {
                        return "West";
                    }
                }).collect(Collectors.toList());

        Map<String, Long> map = new HashMap<>();

        map.put("North", directions.stream().filter(direction -> direction.equals("N")).count());
        map.put("East", directions.stream().filter(direction -> direction.equals("E")).count());
        map.put("South", directions.stream().filter(direction -> direction.equals("S")).count());
        map.put("West", directions.stream().filter(direction -> direction.equals("W")).count());

        Long maxValue = map.values().stream()
                .mapToLong(max -> max)
                .max()
                .orElse(0);
        return map.keySet().stream()
                .filter(key -> map.get(key)
                        .equals(maxValue))
                .findFirst()
                .orElse(null);
    }
}
